# PizzaDjango

## AWS

Deux Instance EC2 sur AWS : 
- Un serveur Jenkins au quel toute les tâches seront automatisées
- Un docker Host la ou le deploiement sera fait

## Jenkins
pipeline : 
    1. Delete l'ancienne repository
    2. Ceckout SCM afin de récuperer la repo gitlab 
    3. Package pour compresser le dossier du projet pour faciliter le deploiement
    4. Envoie du package via SSH au Docker Host
    5. Executer un playbook Ansible permettant d'un premier temps supprimer le conteuneur et l'image créé si ils sont présent pour ensuite créé une image docker et ensuite un conteneur possèdant le projet décompresser puis le deployer via Django 

A chaque PUSH du dev sur sa repo GitLab, la pipline s'enclanche afin d'avoir un envrionnement sécuriser et sans risque à la casse pour tester son application 


## Ansible 

    1. On choise le Docker host comme Hosts cible
    2. Stopper les conteneur existant
    3. Supprimer les conteneur existant 
    4. Supprimer les images existant
    5. Créé uen image docker possédant Django via un dockerfile
    6. Push l'image sur docker hub pour le récuperer si besoins ensuite
    7. Run le conteneur via un docker run


## Docker
Création d'une image docker a partir de Debian:latest avec un update et une Installation de sqlite3 et d'un import du requirements.txt puis de son Installation . Une commande Python3 manage.py runserver se fera ensuite automatiquement.

## Mode d'emploie
Le developeur n'a qu'à push son projet sur son Gitlab après avoir ouvert les serveurs Jenkins et Docker. 
Quand cela sera fait aller sur votre serveur Docker et lancer la commande suivante : 
                    

                            docker exec -it python-django /bin/bash 

Puis lancer la commande suivante : 

                            /bin/bash execute.sh

                            
Maintenant votre serveur Django est lancé vous pouvez utilisez votre application sur votre navigateur web dans l'adresse suivante : 

                            107.21.224.214:8080



