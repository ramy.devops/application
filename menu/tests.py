# dans menu/tests.py

from django.test import TestCase, RequestFactory
from .views import index

class MenuViewTest(TestCase):

    def setUp(self):
        # Crée une instance de RequestFactory()
        self.request_factory = RequestFactory()

    def test_ak_menu_view(self):
        """
        Teste si la page de menu est accessible
        """
        request = self.request_factory.get("/menu/")
        response = index(request)
        self.assertEqual(response.status_code, 200)

