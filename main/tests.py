# dans main/tests.py

from django.test import TestCase, RequestFactory
from .views import index

class MainViewTest(TestCase):

    def setUp(self):
        # Crée une instance de RequestFactory()
        self.request_factory = RequestFactory()

    def test_ak_main_view(self):
        """
        Teste si la page d'accueil est accessible
        """
        request = self.request_factory.get("/")
        response = index(request)
        self.assertEqual(response.status_code, 200)
